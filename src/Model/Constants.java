package Model;

import java.io.File;

/**
 * Project Graphical User Interface for Potree Converter
 *
 * @author Van Heirstraeten Arthur
 * Ecole Polytechnique de Bruxelles
 * 
 * Lisa Department
 *
 * Constants class for the application
 */
public class Constants {
	public static final String APP_NAME = "Panotree";
	public static final String APP_DISPLAY_NAME = "anotree";
	public static final String ALERTPOPUP_NAME = "An error has occured";
	public static final int WINDOW_WIDTH = 1280;
	public static final int WINDOW_HEIGHT = 720;
	
	public static final String currentDirectory = new File("").getAbsolutePath() + "/";
	public static final String configPath = currentDirectory + "config/";
	public static final String configFile = "preferences.txt";
	public static final String PotreePath = currentDirectory + "PotreeConverter/PotreeConverter.exe";
	public static final String logPath = currentDirectory + "logs/";
	public static final String relPathToResources = "/resources/";
	public static final String iconFile = relPathToResources + "icon.png";

}
