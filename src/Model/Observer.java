package Model;

import Controller.BaseController;
import Controller.MenuBarController;
import Controller.OptionBarController;
import View.PreviewBrowser;

/**
 * Project Graphical User Interface for Potree Converter
 *
 * @author Van Heirstraeten Arthur
 * Ecole Polytechnique de Bruxelles
 * 
 * Lisa Department
 *
 * Observer class for communication pass-through
 */
public class Observer {
	
	private OptionBarController optionbarcontroller;
	private MenuBarController menubarcontroller;
	private PreviewBrowser browser;
	private Preferences preferences;
	private Arguments arguments;
	
	public Observer() {	
	}
	
	/**
	 * toggle arguments recovery from optionbar
	 */
	public void notifyUpdateArguments() {
		optionbarcontroller.saveArguments();
	}

	public void setOptionbarcontroller(BaseController optionbarcontroller) {
		this.optionbarcontroller = (OptionBarController)optionbarcontroller;
	}

	public OptionBarController getOptionBarController() {
		return optionbarcontroller;
	}
	
	public void setMenubarcontroller(BaseController menubarcontroller) {
		this.menubarcontroller = (MenuBarController)menubarcontroller;
	}
	
	public void setPreferences(Preferences preferences) {
		this.preferences = preferences;
	}
	
	public Preferences getPreferences() {
		return preferences;
	}
	
	public void setArguments(Arguments arguments) {
		this.arguments = arguments;
	}
	
	public Arguments getArguments() {
		return arguments;
	}
	
	public void setBrowser(PreviewBrowser browser) {
		this.browser = browser;
	}
	
	public PreviewBrowser getBrowser() {
		return browser;
	}
	
}
