package Model;

/**
 * Project Graphical User Interface for Potree Converter
 *
 * @author Van Heirstraeten Arthur
 * Ecole Polytechnique de Bruxelles
 * 
 * Lisa Department
 *
 * Constants class for the command arguments
 */
public class ArgumentsConstants {
	public static final String OUTPUT_AS_HTML = "-p"; // "--generate-page"
	public static final String OUTPUT_DIRECTORY = "-o"; // "--outdir"
	public static final String SPACING = "-s"; // "--spacing"
	public static final String SPACING_BY_DIAGONAL_FRACTION = "-d"; // "--spacing-by-diagonal-fraction"
	public static final String LEVELS = "-l"; // "--levels"
	public static final String INPUT_FORMAT = "-f"; // "--input-format"
	public static final String COLOR_RANGE = "--color-range";
	public static final String INTENSITY_RANGE = "--intensity-range";
	public static final String OUTPUT_FORMAT = "--output-format";
	public static final String OUTPUT_ATTRIBUTES = "-a"; // "--output-attributes"
	public static final String SCALE = "--scale";
	public static final String AABB = "--aabb";
	public static final String INCREMENTAL = "--incremental";
	public static final String OVERWRITE = "--overwrite";
	public static final String SOURCE_LISTING_ONLY = "--source-listing-only";
	public static final String PROJECTION = "--projection";
	public static final String LIST_OF_FILES = "--list-of-files";
	public static final String SOURCE = "--source"; // Appears three times in the doc, only once in this project
	public static final String TITLE = "--title";
	public static final String DESCRIPTION = "--description";
	public static final String EDL_ENABLED = "--edl-enabled";
	public static final String SHOW_SKYBOX = "--show-skybox";
	public static final String MATERIAL = "--material";
	public static final String STORE_SIZE = "--store-size";
	public static final String FLUSH_LIMIT = "--flush-limit";
	

	public static final String OUTPUT_ATTRIBUTES_RGB = "RGB"; 
	public static final String OUTPUT_ATTRIBUTES_INTENSITY = "INTENSITY"; 
	public static final String OUTPUT_ATTRIBUTES_CLASSIFICATION = "CLASSIFICATION"; 
}
