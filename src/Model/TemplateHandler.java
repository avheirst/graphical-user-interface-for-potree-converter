package Model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Project Graphical User Interface for Potree Converter
 *
 * @author Van Heirstraeten Arthur
 * Ecole Polytechnique de Bruxelles
 * 
 * Lisa Department
 *
 * Template handler class
 * Handles the change of template of html files
 */
public class TemplateHandler {
	
	private String file_path;
	private String fov;
	private String point_budget;
	private String language;
	private String background;
	private String point_size;

	private ArrayList<String> menus;

	public TemplateHandler(String file_path) {
		this.file_path = file_path;
	}
	
	/**
	 * Apply the user parameters with specific values
	 * @param fov
	 * @param point_budget
	 * @param language
	 * @param background
	 * @param point_size
	 */
	public void applyChanges(String fov, String point_budget, String language, String background, String point_size) {
		this.fov = "		viewer.setFOV(" + fov + ");";
		this.point_budget = "		viewer.setPointBudget(" + point_budget + "); // nombre de points loades (trouver le nbr de point max attention doit etre compile pour)";
		this.language = "			viewer.setLanguage('" + language + "'); // choix de la Langue : en,fr,de,jp";
		this.background = "		viewer.setBackground(\"" + background + "\"); // [\"skybox\", \"gradient\", \"black\", \"white\"];";
		this.point_size = "			material.size = " + point_size + ";";
	}
	
	/**
	 * Toggle the different menu opening or showing with user input
	 * 
	 * @param hide_appearance_menu
	 * @param hide_tools_menu
	 * @param hide_scene_menu
	 * @param hide_classification_menu
	 * @param hide_about_menu
	 * @param close_appearance_menu
	 * @param close_tools_menu
	 * @param close_scene_menu
	 * @param close_classification_menu
	 * @param close_about_menu
	 * @param toggle_sidebar
	 */
	public void toggleMenus(Boolean hide_appearance_menu, Boolean hide_tools_menu, Boolean hide_scene_menu, Boolean hide_classification_menu,
			Boolean hide_about_menu, Boolean close_appearance_menu, Boolean close_tools_menu, Boolean close_scene_menu,
			Boolean close_classification_menu, Boolean close_about_menu, Boolean toggle_sidebar) {
		
		menus = new ArrayList<String>();
		
		if (close_appearance_menu) {menus.add("//			$(\"#menu_appearance\").next().show();");
		}else {menus.add("			$(\"#menu_appearance\").next().show();");}
		if (close_tools_menu) {menus.add("//			$(\"#menu_tools\").next().show();");
		}else {menus.add("			$(\"#menu_tools\").next().show();");}
		if (close_scene_menu) {menus.add("//			$(\"#menu_scene\").next().show();");
		}else {menus.add("			$(\"#menu_scene\").next().show();");}
		if (close_classification_menu) {menus.add("//			$(\"#menu_classification\").next().show();");
		}else {menus.add("			$(\"#menu_classification\").next().show();");}
		if (close_about_menu) {menus.add("//			$(\"#menu_about\").next().show();");
		}else {menus.add("			$(\"#menu_about\").next().show();");}
		
		if (hide_appearance_menu) {menus.add("			$(\"#menu_appearance\").hide();");
		}else {menus.add("//			$(\"#menu_appearance\").hide();");}
		if (hide_tools_menu) {menus.add("			$(\"#menu_tools\").hide();");
		}else {menus.add("//			$(\"#menu_tools\").hide();");}
		if (hide_scene_menu) {menus.add("			$(\"#menu_scene\").hide();");
		}else {menus.add("//			$(\"#menu_scene\").hide();");}
		if (hide_classification_menu) {menus.add("			$(\"#menu_classification\").hide();");
		}else {menus.add("//			$(\"#menu_classification\").hide();");}
		if (hide_about_menu) {menus.add("			$(\"#menu_about\").hide();");
		}else {menus.add("//			$(\"#menu_about\").hide();");}
		
		if (toggle_sidebar) {menus.add("			viewer.toggleSidebar();");
		}else {menus.add("//			viewer.toggleSidebar();");}
	}
	
	/**
	 * write the given lines into the output file
	 * @param output_file_path
	 * @param lines
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private void writeToFile(String output_file_path, ArrayList<String> lines) throws FileNotFoundException, IOException {
		File file = new File(output_file_path);
		FileOutputStream fos = new FileOutputStream(file);
		for (String elem:lines) {
			byte[] strToBytes = (elem+"\n").getBytes();
			fos.write(strToBytes);
		}
		fos.close();
	}
	
	/**
	 * Parse the pointcloud file and call writeToFile(output_file_path, lines)
	 * for writing the output file
	 */
	public void writeToFile(){
		try {
			File file = new File(file_path); 
			Scanner sc = new Scanner(file);
			ArrayList<String> file_content = new ArrayList<String>();
			String line;
	  
			while (sc.hasNextLine()) {
				line = sc.nextLine();
				if (line.contains("viewer.setFOV")) {
					file_content.add(fov);
				}else if(line.contains("viewer.setPointBudget")){
					file_content.add(point_budget);
				}else if(line.contains("viewer.setBackground")) {
					file_content.add(background);
				}else if(line.contains("viewer.setLanguage")) {
					file_content.add(language);
					while(sc.hasNextLine()&& !line.contains("viewer.toggleSidebar")){
						line = sc.nextLine();
					}
					file_content.addAll(menus);					
				}else if(line.contains("material.size =")) {
					file_content.add(point_size);
				}
				else {
					file_content.add(line);
				}
			}
			sc.close();
			writeToFile(file_path,file_content);
		}catch(FileNotFoundException e) {
			e.printStackTrace();//TODO
		}catch(IOException e) {
			e.printStackTrace();//TODO
		}
	}

}
