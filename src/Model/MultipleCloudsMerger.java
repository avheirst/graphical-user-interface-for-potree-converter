package Model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Project Graphical User Interface for Potree Converter
 *
 * @author Van Heirstraeten Arthur
 * Ecole Polytechnique de Bruxelles
 * 
 * Lisa Department
 *
 * Handles merging multiple clouds into one html page
 * main_file_name is the page from which properties are recovered except clouds ones
 */
public class MultipleCloudsMerger {
	
	private ArrayList<String> point_clouds = new ArrayList<String>();
	private String main_file_name;
	
	public MultipleCloudsMerger(Preferences pref, String main_file_name) {
		this.main_file_name = main_file_name;
	}
	
	/**
	 * Parse the given file to extract only lines containing the pointcloud informations
	 * @param file_name
	 */
	public void addCloud(String file_name) {
		try {
			extractNewPointcloud(file_name);
		}catch(FileNotFoundException e) {
			e.printStackTrace();//TODO: handle exception
		}
	}
	
	/**
	 * Merge all parsed files into given output file
	 * @param output_file_name
	 */
	public void merge(String output_file_name) {
		try {
			mergeFiles(main_file_name, output_file_name);
		}catch(FileNotFoundException e) {
			e.printStackTrace();//TODO
		}catch(IOException e) {
			e.printStackTrace();//TODO
		}
	}

	/**
	 * Parse the given file to extract only lines containing the pointcloud informations
	 * @param file_path
	 * @throws FileNotFoundException
	 */
	private void extractNewPointcloud(String file_path) throws FileNotFoundException {
		File file = new File(file_path); 
	    Scanner sc = new Scanner(file);
	    String line;
	  
	    while (sc.hasNextLine()) {
	    	line = sc.nextLine();
	    	if (line.contains("Potree.loadPointCloud")) {
	    		while(sc.hasNextLine()&& !line.contains("});")){
	    		    if (line.contains("// Annotations section")) {
						// If an annotation is present, copy it
							
						while(sc.hasNextLine() && !line.contains("// End of annotations section")){
							// Search for end of annotation keyword
							point_clouds.add(line);
							line = sc.nextLine();
						}
						//point_clouds.add(line);
						//line = sc.nextLine();
	    		    }
	    			point_clouds.add(line);
	    			line = sc.nextLine();
	    		}
	    		point_clouds.add(line);
	    		point_clouds.add("\n");
	    	}
	    }
	    sc.close();
	}
	
	/**
	 * write the given lines into the output file
	 * @param output_file_path
	 * @param lines
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private void writeToFile(String output_file_path, ArrayList<String> lines) throws FileNotFoundException, IOException {
		File file = new File(output_file_path);
		FileOutputStream fos = new FileOutputStream(file);
		for (String elem:lines) {
			byte[] strToBytes = (elem+"\n").getBytes();
			fos.write(strToBytes);
		}
		fos.close();
	}
	
	/**
	 * Parse the main file to insert the other clouds at the right place
	 * @param main_file_path
	 * @param output_file_path
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private void mergeFiles(String main_file_path, String output_file_path) throws FileNotFoundException, IOException {
		System.out.println("Main file: " + main_file_path);
		System.out.println("Output file: " + output_file_path);
		File file = new File(main_file_path); 
	    Scanner sc = new Scanner(file);
	    ArrayList<String> file_content = new ArrayList<String>();
	    String line;
	  
	    while (sc.hasNextLine()) {
	    	line = sc.nextLine();
	    	file_content.add(line);
	    	if (line.contains("Potree.loadPointCloud")) {
	    		while(sc.hasNextLine()&& !line.contains("});")){
	    			if (line.contains("// Annotations section")) {
						// If an annotation is present, copy it
							
						while(sc.hasNextLine() && !line.contains("// End of annotations section")){
							// Search for end of annotation keyword
							file_content.add(line);
							line = sc.nextLine();
						}
						file_content.add(line);
	    		    }
	    			line = sc.nextLine();
	    			file_content.add(line);
	    		}
	    		file_content.add("\n");
	    		file_content.addAll(point_clouds);
	    	}
	    }
	    sc.close();
	    writeToFile(output_file_path,file_content);
	}
}
