package Model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Project Graphical User Interface for Potree Converter
 *
 * @author Van Heirstraeten Arthur
 * Ecole Polytechnique de Bruxelles
 * 
 * Lisa Department
 *
 * Handler for pointcloud annotations
 */
public class AnnotationsHandler {
	
	private String file_path;
	private ArrayList<String> annotations;

	public AnnotationsHandler(String file_path) {
		this.file_path = file_path;
		reset();
	}
	
	/**
	 * Reset the annotations
	 */
	public void reset() {
		annotations = new ArrayList<String>();
		annotations.add("			// Annotations section");
		annotations.add("			{");
	}
	
	/**
	 * Create a new annotation by formatting the inputs to html code
	 * @param annotation_name
	 * @param title
	 * @param position
	 * @param description
	 */
	public void addAnnotation(String annotation_name, String title, String position, String description) {
		annotation_name = annotation_name.replaceAll("\\ ", "");
		description = description.replaceAll("\n", "<br>");
		
		annotations.add("				{");
		annotations.add("					let " + annotation_name + " = new Potree.Annotation({");
		annotations.add("						position: [" + position + "],");
		annotations.add("						\"title\": \"" + title + "\",");
		annotations.add("						\"description\": `" + description + "`");
		annotations.add("					});");
		annotations.add("					viewer.scene.annotations.add(" + annotation_name + ");");
		annotations.add("				}");
		annotations.add("\n");
	}
	
	/**
	 * write the given lines into the output file
	 * @param output_file_path
	 * @param lines
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private void writeToFile(String output_file_path, ArrayList<String> lines) throws FileNotFoundException, IOException {
		File file = new File(output_file_path);
		FileOutputStream fos = new FileOutputStream(file);
		for (String elem:lines) {
			byte[] strToBytes = (elem+"\n").getBytes();
			fos.write(strToBytes);
		}
		fos.close();
	}
	
	/**
	 * Parse the pointcloud file and call writeToFile(output_file_path, lines)
	 * for writing the output file
	 */
	public void writeToFile(){
		try {
			annotations.add("			}");
			annotations.add("			// End of annotations section");
			File file = new File(file_path); 
			Scanner sc = new Scanner(file);
			ArrayList<String> file_content = new ArrayList<String>();
			String line;
			Boolean annotations_added = false;
	  
			while (sc.hasNextLine()) {
				line = sc.nextLine();
				
				if (line.contains("Potree.loadPointCloud") && !annotations_added) {
					// Search for the first pointcloud	
					
					while(sc.hasNextLine() && !line.contains("});")){
						// Search the end of the pointcloud
						
						if (line.contains("// Annotations section")) {
							// If an annotation is already present, just copy it
							
							annotations.remove(0);
							while(sc.hasNextLine() && !line.contains("// End of annotations section")){
								// Search for end of annotation keyword
								file_content.add(line);
								line = sc.nextLine();
							}
							line = sc.nextLine();							
						}else {
							file_content.add(line);
							line = sc.nextLine();
						}
					}
					file_content.addAll(annotations);
					annotations_added = true;
				}
				file_content.add(line);
			}
			sc.close();
			writeToFile(file_path,file_content);
		}catch(FileNotFoundException e) {
			e.printStackTrace();//TODO
		}catch(IOException e) {
			e.printStackTrace();//TODO
		}
	}
	

}
