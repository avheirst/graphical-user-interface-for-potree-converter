package Model;

import Model.ArgumentsConstants;

/**
 * Project Graphical User Interface for Potree Converter
 *
 * @author Van Heirstraeten Arthur
 * Ecole Polytechnique de Bruxelles
 * 
 * Lisa Department
 *
 * stores all parameters user selected for conversion
 * instance dependent
 */
public class Arguments {
	
	private Boolean output_as_html;
	private Boolean spacing;
	private Boolean diagonal_fraction;
	private Boolean levels;
	private Boolean input_format;
	private Boolean color_range;
	private Boolean intensity_range;
	private Boolean output_format;
	private Boolean output_attributes;
	private Boolean output_attributes_RGB;
	private Boolean output_attributes_INTENSITY;
	private Boolean output_attributes_CLASSIFICATION;
	private Boolean scale;
	private Boolean aabb;
	private Boolean incremental;
	private Boolean overwrite;
	private Boolean source_listing_only;
	private Boolean projection;
	private Boolean list_of_files;
	private Boolean title;
	private Boolean description;
	private Boolean edl_enabled;
	private Boolean show_skybox;
	private Boolean material;
	private Boolean store_size;
	private Boolean flush_limit;
	
	private String file_to_convert;
	private String output_folder;
	private String html_file_name;
	private String spacing_args;
	private String diagonal_fraction_args;
	private String levels_args;
	private String input_format_args;
	private String color_range_args;
	private String intensity_range_args;
	private String output_format_args;
	private String scale_args;
	private String aabb_min_args;
	private String aabb_max_args;
	private String projection_args;
	private String list_of_files_name;
	private String title_args;
	private String description_args;
	private String material_args;
	private String store_size_args;
	private String flush_limit_args;
	
	private Observer observer;

	public Arguments(Observer observer) {
		this.observer = observer;
		spacing = diagonal_fraction = levels = input_format = color_range = intensity_range = output_format = output_attributes = 
				output_attributes_RGB = output_attributes_INTENSITY = output_attributes_CLASSIFICATION = scale = aabb = incremental =
				overwrite = source_listing_only = projection = list_of_files = title = description = edl_enabled =
				show_skybox = material = store_size = flush_limit = false; // Initialize all parameters to false
		output_as_html = true;
	}
	
	/**
	 * Converts arguments and preferences to a command to be executed
	 * @param html_filename_overwrite the html filename requested (useful for multiple files selected at once)
	 * @return the command
	 */
	public String getCommand(String html_filename_overwrite) {
		String command = observer.getPreferences().potree_location + " ";
		command += "\"" + file_to_convert + "\" ";
		
		if (!output_folder.equals("")) {
			command += ArgumentsConstants.OUTPUT_DIRECTORY + " " + output_folder + " ";
		}else {
			command += ArgumentsConstants.OUTPUT_DIRECTORY + " " + observer.getPreferences().output_folder + " ";
		}
		
		if (output_as_html && html_filename_overwrite != null) {
			command += ArgumentsConstants.OUTPUT_AS_HTML + " " + html_filename_overwrite + " ";
		}
		if (spacing && spacing_args != null) {
			command += ArgumentsConstants.SPACING + " " + spacing_args + " ";
		}
		if (diagonal_fraction && diagonal_fraction_args != null) {
			command += ArgumentsConstants.SPACING_BY_DIAGONAL_FRACTION + " " + diagonal_fraction_args + " ";
		}
		if (levels && levels_args != null) {
			command += ArgumentsConstants.LEVELS + " " + levels_args + " ";
		}
		if (input_format && input_format_args != null) {
			command += ArgumentsConstants.INPUT_FORMAT + " " + input_format_args + " ";
		}
		if (color_range && color_range_args != null) {
			command += ArgumentsConstants.COLOR_RANGE + " " + color_range_args + " ";
		}
		if (intensity_range && intensity_range_args != null) {
			command += ArgumentsConstants.INTENSITY_RANGE + " " + intensity_range_args + " ";
		}
		if (output_format && output_format_args != null) {
			command += ArgumentsConstants.OUTPUT_FORMAT + " " + output_format_args + " ";
		}
		if (output_attributes && (output_attributes_RGB || output_attributes_INTENSITY || output_attributes_CLASSIFICATION)) {
			command += ArgumentsConstants.OUTPUT_ATTRIBUTES + " ";
			if (output_attributes_RGB) {
				command += ArgumentsConstants.OUTPUT_ATTRIBUTES_RGB + " ";
			}
			if (output_attributes_INTENSITY) {
				command += ArgumentsConstants.OUTPUT_ATTRIBUTES_INTENSITY + " ";
			}
			if (output_attributes_CLASSIFICATION) {
				command += ArgumentsConstants.OUTPUT_ATTRIBUTES_CLASSIFICATION + " ";
			}
		}
		if (scale && scale_args != null) {
			command += ArgumentsConstants.SCALE + " " + scale_args + " ";
		}
		if (aabb && aabb_min_args != null && aabb_max_args != null) {
			command += ArgumentsConstants.AABB + " \"" + aabb_min_args + " " + aabb_max_args + "\" ";
		}
		if (incremental) {
			command += ArgumentsConstants.INCREMENTAL + " ";
		}
		if (overwrite) {
			command += ArgumentsConstants.OVERWRITE +  " ";
		}
		if (source_listing_only) {
			command += ArgumentsConstants.SOURCE_LISTING_ONLY + " ";
		}
		if (projection && projection_args != null) {
			command += ArgumentsConstants.PROJECTION + " " + projection_args + " ";
		}
		if (list_of_files && list_of_files_name != null) {
			command += ArgumentsConstants.LIST_OF_FILES + " " + list_of_files_name + " ";
		}
		if (title && title_args != null) {
			command += ArgumentsConstants.TITLE + " " + title_args + " ";
		}
		if (description && description_args != null) {
			command += ArgumentsConstants.DESCRIPTION + " \"" + description_args + "\" ";
		}
		if (edl_enabled) {
			command += ArgumentsConstants.EDL_ENABLED + " ";
		}
		if (show_skybox) {
			command += ArgumentsConstants.SHOW_SKYBOX + " ";
		}
		if (material && material_args != null) {
			command += ArgumentsConstants.MATERIAL + " \"" + material_args + "\" ";
		}
		if (store_size && store_size_args != null) {
			command += ArgumentsConstants.STORE_SIZE + " " + store_size_args + " ";
		}if (flush_limit && flush_limit_args != null) {
			command += ArgumentsConstants.FLUSH_LIMIT + " " + flush_limit_args + " ";
		}
		
		return command;
	}
	
	public String getConversionCommand() {
		String command = (observer.getPreferences().lastools_bin_location +"\\e572las.exe" + " ");
		command += "\"" + file_to_convert + "\"";
		return command;
		
	}
	
	public String getHtmlFileName() {
		return html_file_name;
	}
	
	public String getFileToConvert() {
		return file_to_convert;
	}
	
	public Boolean getIncremental() {
		return incremental;
	}
	
	public void setObserver(Observer observer) {
		this.observer = observer;
	}
	
	public void setFileToConvert(String file_to_convert) {
		this.file_to_convert = file_to_convert;
	}
	
	public void setOutputFolder(String output_folder) {
		this.output_folder = output_folder;
	}
	
	public void setOutputAsHtml(Boolean output_as_html) {
		this.output_as_html = output_as_html;
	}
	
	public void setHtmlFileName(String html_file_name) {
		this.html_file_name = html_file_name;
	}

	public void setSpacing(Boolean spacing) {
		this.spacing = spacing;
	}

	public void setSpacingArgs(String spacing_args) {
		this.spacing_args = spacing_args;
	}

	public void setDiagonalFraction(Boolean diagonal_fraction) {
		this.diagonal_fraction = diagonal_fraction;
	}

	public void setDiagonalFractionArgs(String diagonal_fraction_args) {
		this.diagonal_fraction_args = diagonal_fraction_args;
	}

	public void setLevels(Boolean levels) {
		this.levels = levels;
	}

	public void setLevelsArgs(String levels_args) {
		this.levels_args = levels_args;
	}

	public void setInputFormat(Boolean input_format) {
		this.input_format = input_format;
	}

	public void setInputFormatArgs(String input_format_args) {
		this.input_format_args = input_format_args;
	}

	public void setColorRange(Boolean color_range) {
		this.color_range = color_range;
	}

	public void setColorRangeArgs(String color_range_args) {
		this.color_range_args = color_range_args;
	}

	public void setIntensityRange(Boolean intensity_range) {
		this.intensity_range = intensity_range;
	}

	public void setIntensityRangeArgs(String intensity_range_args) {
		this.intensity_range_args = intensity_range_args;
	}

	public void setOutputFormat(Boolean output_format) {
		this.output_format = output_format;
	}

	public void setOutputFormatArgs(String output_format_args) {
		this.output_format_args = output_format_args;
	}

	public void setOutputAttributes(Boolean output_attributes) {
		this.output_attributes = output_attributes;
	}
	
	public void setOutputAttributesRGB(Boolean output_attributes_RGB) {
		this.output_attributes_RGB = output_attributes_RGB;
	}
	
	public void setOutputAttributesINTENSITY(Boolean output_attributes_INTENSITY) {
		this.output_attributes_INTENSITY = output_attributes_INTENSITY;
	}
	
	public void setOutputAttributesCLASSIFICATION(Boolean output_attributes_CLASSIFICATION) {
		this.output_attributes_CLASSIFICATION = output_attributes_CLASSIFICATION;
	}

	public void setScale(Boolean scale) {
		this.scale = scale;
	}

	public void setScaleArgs(String sclare_args) {
		this.scale_args = sclare_args;
	}

	public void setAabb(Boolean aabb) {
		this.aabb = aabb;
	}

	public void setAabbMinArgs(String aabb_min_args) {
		this.aabb_min_args = aabb_min_args;
	}

	public void setAabbMaxArgs(String aabb_max_args) {
		this.aabb_max_args = aabb_max_args;
	}

	public void setIncremental(Boolean incremental) {
		this.incremental = incremental;
	}

	public void setOverwrite(Boolean overwrite) {
		this.overwrite = overwrite;
	}

	public void setSourceListingOnly(Boolean source_listing_only) {
		this.source_listing_only = source_listing_only;
	}

	public void setProjection(Boolean projection) {
		this.projection = projection;
	}

	public void setProjectionArgs(String projection_args) {
		this.projection_args = projection_args;
	}

	public void setListOfFiles(Boolean list_of_files) {
		this.list_of_files = list_of_files;
	}

	public void setListOfFilesName(String list_of_files_name) {
		this.list_of_files_name = list_of_files_name;
	}

	public void setTitle(Boolean title) {
		this.title = title;
	}

	public void setTitleArgs(String title_args) {
		this.title_args = title_args;
	}

	public void setDescription(Boolean description) {
		this.description = description;
	}

	public void setDescriptionArgs(String description_args) {
		this.description_args = description_args;
	}

	public void setEdlEnabled(Boolean edl_enabled) {
		this.edl_enabled = edl_enabled;
	}

	public void setShowSkybox(Boolean show_skybox) {
		this.show_skybox = show_skybox;
	}

	public void setMaterial(Boolean material) {
		this.material = material;
		if (material_args == null) {
			material_args = "";
		}
	}
	
	/**
	 * Add a material to the materials list
	 * @param material_arg
	 */
	public void addMaterialArg(String material_arg) {
		if (material_args.equals("")) {
			material_args = material_arg;
		}else {
			material_args += ", " + material_arg;
		}
	}
	
	/**
	 * remove a material from the materials list
	 * @param material_arg
	 */
	public void removeMaterialArg(String material_arg) {
		String new_material_args = material_args.replace(", " + material_arg, "");
		if (new_material_args.equals(material_args)) {
			new_material_args = material_args.replace(material_arg + ", ", "");
		}
		if (new_material_args.equals(material_args)) {
			new_material_args = material_args.replace(material_arg, "");
		}
		material_args = new_material_args;
	}

	public void setMaterialArgs(String material_args) {
		this.material_args = material_args;
	}
	
	public void setStoreSize(Boolean store_size) {
		this.store_size = store_size;
	}

	public void setStoreSizeArgs(String store_size_args) {
		this.store_size_args = store_size_args;
	}
	
	public void setFlushLimit(Boolean flush_limit) {
		this.flush_limit = flush_limit;
	}

	public void setFlushLimitArgs(String flush_limit_args) {
		this.flush_limit_args = flush_limit_args;
	}

}
