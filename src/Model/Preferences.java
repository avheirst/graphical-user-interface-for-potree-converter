package Model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 * Project Graphical User Interface for Potree Converter
 *
 * @author Van Heirstraeten Arthur
 * Ecole Polytechnique de Bruxelles
 * 
 * Lisa Department
 *
 * Preferences class
 * Preferences are stored in a separate file on the disk
 */
public class Preferences {
	public String potree_location;
	public String output_folder;
	public String lastools_bin_location;
	public String default_html_name;
	public String default_browser_page;
	public int converting_threads_amount;
	
	private Properties prop = new Properties();
	private String file_name = Constants.configPath + Constants.configFile;
	
	public Preferences() {
		getPreferencesFromConfigFile();
	}
	
	/**
	 * Recover data from file and stores into class variables
	 */
	private void getPreferencesFromConfigFile() {
		openConfigFile();
		
		potree_location = prop.getProperty("potreeLocation");
		if (potree_location == null) {
			potree_location = Constants.PotreePath;
		}
		output_folder = prop.getProperty("outputFolder");	
		lastools_bin_location = prop.getProperty("lastoolsBinLocation");
		default_html_name = prop.getProperty("defaultHtmlPageName");
		default_browser_page = prop.getProperty("defaultBrowserPage");
		String converting_threads_amount_str = prop.getProperty("convertingThreadsAmount");
		if (converting_threads_amount_str == null) {
			converting_threads_amount = Runtime.getRuntime().availableProcessors();
		}else {
			converting_threads_amount = Integer.valueOf(converting_threads_amount_str);
		}
	}
	
	/**
	 * Connects property to the config file
	 * 
	 * Create file if not exists		  
	 */
	private void openConfigFile() {
		File directory = new File(Constants.configPath);
		File file = new File(file_name);
		if (!directory.exists()) {
			directory.mkdir();
		}
		try {
			file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		InputStream inputstream = null;
		try {
		    inputstream = new FileInputStream(file);
		} catch (FileNotFoundException ex) {
		    ex.printStackTrace();
		}
		try {
		    prop.load(inputstream);
		} catch (IOException ex) {
		    ex.printStackTrace();
		}
	}
	
	/**
	 * Update a value in the config file with arguments given 
	 * 
	 * Throws an exception if file is not present
	 */
	public void changePreference(String key, String value){
		OutputStream outputstream = null;
		try {
		    outputstream = new FileOutputStream(file_name);
		} catch (FileNotFoundException ex) {
		    ex.printStackTrace();
		}
		prop.put(key, value);
		try {
			prop.store(outputstream, null);
		} catch (IOException ex) {
		    ex.printStackTrace();
		}
	}
	
	/**
	 * update preferences from config file
	 */
	public void updatePreferences() {
		getPreferencesFromConfigFile();
	}

}
