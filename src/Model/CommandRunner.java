package Model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import View.AlertPopup;

/**
 * Project Graphical User Interface for Potree Converter
 *
 * @author Van Heirstraeten Arthur
 * Ecole Polytechnique de Bruxelles
 * 
 * Lisa Department
 * 
 * Class handling the execution of a command
 * TODO: make it able to run on Linux
 */
public abstract class CommandRunner{
	
	/**
	 * Run the specified command on the terminal
	 * If the conversion fails, terminal output is written into a log file
	 * @param file_name
	 * @param command
	 * @return success of conversion
	 */
	public static Boolean runCommand(String file_name, String command) {
		Boolean conversion_success_state = false;
		ArrayList<String> terminal_output = new ArrayList<String>();
		try {
			Process proc = Runtime.getRuntime().exec(command);
			BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
		    String line = "";
		    while((line = reader.readLine()) != null) {
		    	terminal_output.add(line);
		    	System.out.print(line + "\n");
		    	if (line.equals("conversion finished")) {
		    		conversion_success_state = true;
		    	}
		    }
		    proc.waitFor();
			if (!conversion_success_state) {
				writeToFile(Constants.logPath + file_name + ".log", terminal_output);
			}
		} catch (IOException e) {
			e.printStackTrace();
			AlertPopup.displayRuntimeError(e);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return conversion_success_state;
	}
	
	/**
	 * write the given lines into the output file
	 * @param output_file_path
	 * @param lines
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private static void writeToFile(String output_file_path, ArrayList<String> lines) throws FileNotFoundException, IOException {
		File file = new File(output_file_path);
		FileOutputStream fos = new FileOutputStream(file);
		for (String elem:lines) {
			byte[] strToBytes = (elem+"\n").getBytes();
			fos.write(strToBytes);
		}
		fos.close();
	}
	
	/**
	 * Run the specified conversion command on the terminal
	 * @param command
	 * @return success of conversion
	 */
	public static Boolean runConversionCommand(String command) {
		System.out.println("Converting to las format...");
		Boolean conversion_success_state = false;
		try {
			Process proc = Runtime.getRuntime().exec(command);
		    proc.waitFor(); 
		    conversion_success_state =true;
		} catch (IOException e) {
			e.printStackTrace();
			AlertPopup.displayRuntimeError(e);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return conversion_success_state;
	}

}
