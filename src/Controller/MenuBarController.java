package Controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import View.PreferencesWindow;
import javafx.fxml.FXML;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

/**
 * Project Graphical User Interface for Potree Converter
 *
 * @author Van Heirstraeten Arthur
 * Ecole Polytechnique de Bruxelles
 * 
 * Lisa Department
 *
 * Controller for menubar
 * Uses JavaFX
 */
public class MenuBarController extends BaseController {
	
	public MenuBarController() {
	}
	
	/**
	 * Open the preferences window
	 */
	@FXML
	public void openPreferences() {
		new PreferencesWindow(observer.getPreferences());
	}
	
	/**
	 * Open a dialog for updating url opened in the in-app browser
	 */
	@FXML
	public void openUrl() {
		observer.getBrowser().openNewURL();
	}
	
	/**
	 * Refresh in-app browser
	 */
	@FXML
	public void refreshPreview() {
		observer.getBrowser().refresh();
	}	
	
	/**
	 * Toggle a dialog for the user to choose one or multiple files to open 
	 * in the application for further conversion
	 */
	@FXML
	public void fileBrowse() {
		FileChooser filechooser = new FileChooser();
		filechooser.setInitialDirectory(new File(System.getProperty("user.home")));
		filechooser.setTitle("Select file to convert");
		List<File> selected_files = new ArrayList<File>();
		selected_files = filechooser.showOpenMultipleDialog(stage);
		observer.getOptionBarController().setSelectedFiles(selected_files);
		
	}
	
	/**
	 * Toggle a dialog for the user to choose a folder to open 
	 * in the application for further conversion
	 */
	@FXML
	public void folderBrowse() {
		DirectoryChooser directorychooser = new DirectoryChooser();
		directorychooser.setInitialDirectory(new File(System.getProperty("user.home")));
		directorychooser.setTitle("Select a folder to convert");
		File dir = directorychooser.showDialog(stage);
		List<File> selected_files = new ArrayList<File>();
		selected_files.add(dir);
		observer.getOptionBarController().setSelectedFiles(selected_files);
	}	
	
}
