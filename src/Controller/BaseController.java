package Controller;

import Model.Observer;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * Project Graphical User Interface for Potree Converter
 *
 * @author Van Heirstraeten Arthur
 * Ecole Polytechnique de Bruxelles
 * 
 * Lisa Department
 *
 * Abstract class for controllers
 */
public abstract class BaseController {
	
	protected Observer observer;	
	protected Stage stage;
	@FXML private AnchorPane pane;
	
	public BaseController() {
	}
	
	/**
	 * Bind width of the pane to match window width
	 * @param stage
	 */
	public void bindWidth(Stage stage) {
		this.stage = stage;
		pane.prefWidthProperty().bind(stage.widthProperty());
	}
	
	/**
	 * Bind height of the pane to match window height
	 * @param stage
	 */
	public void bindHeight(Stage stage) {
		this.stage = stage;
		pane.prefHeightProperty().bind(stage.heightProperty());
	}
	
	/**
	 * Link to main observer
	 * @param observer
	 */
	public void setObserver(Observer observer) {
		this.observer = observer;
	}
		

}
