package Controller;

import java.io.File;

import View.AlertPopup;
import View.PreferencesWindow;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * Project Graphical User Interface for Potree Converter
 *
 * @author Van Heirstraeten Arthur
 * Ecole Polytechnique de Bruxelles
 * 
 * Lisa Department
 *
 * Controller for preference window
 */
public class PreferencesWindowController {
	
	private Stage stage = new Stage();	
	private PreferencesWindow preferenceswindow;
	
	@FXML private TextField potreelocationTextField = new TextField();
	@FXML private TextField outputfolderTextField = new TextField();
	@FXML private TextField lastoolsbinlocationTextField = new TextField();
	@FXML private TextField htmlNameTextField = new TextField();
	@FXML private TextField browserTextField = new TextField();
	@FXML private TextField convertingThreadsTextField = new TextField();
	
	public PreferencesWindowController() {
	}
	
	/**
	 * Initialize input fields with value stored in Preferences object
	 */
	public void initializeFields() {
		potreelocationTextField.setText(preferenceswindow.getPref().potree_location);
		outputfolderTextField.setText(preferenceswindow.getPref().output_folder);
		lastoolsbinlocationTextField.setText(preferenceswindow.getPref().lastools_bin_location);
		htmlNameTextField.setText(preferenceswindow.getPref().default_html_name);
		browserTextField.setText(preferenceswindow.getPref().default_browser_page);
		convertingThreadsTextField.setText(String.valueOf(preferenceswindow.getPref().converting_threads_amount));
		
	}
	
	/**
	 * Toggle a dialog for the user to locate Potree.exe
	 */
	@FXML
	public void browsePotree() {
		FileChooser filechooser = new FileChooser();
		filechooser.setInitialDirectory(new File(System.getProperty("user.home")));
		filechooser.setTitle("Select Potree.exe");
		File file = filechooser.showOpenDialog(stage);
		if (file!=null) {
			potreelocationTextField.setText(file.getAbsolutePath());
		}
	}
	
	/**
	 * Toggle a dialog for the user to set the default output folder
	 */
	@FXML
	public void browseOutputFolder() {
		DirectoryChooser directorychooser = new DirectoryChooser();
		directorychooser.setInitialDirectory(new File(System.getProperty("user.home")));
		directorychooser.setTitle("Select default output folder");
		File dir = directorychooser.showDialog(stage);
		if (dir!=null) {
			outputfolderTextField.setText(dir.getAbsolutePath());
		}
	}
	
	/**
	 * Toggle a dialog for the user to locate lastools bin folder
	 */
	@FXML
	public void browseLastoolsFolder() {
		DirectoryChooser directorychooser = new DirectoryChooser();
		directorychooser.setInitialDirectory(new File(System.getProperty("user.home")));
		directorychooser.setTitle("Select lastools bin folder");
		File dir = directorychooser.showDialog(stage);
		if (dir!=null) {
			lastoolsbinlocationTextField.setText(dir.getAbsolutePath());
		}
	}
	
	/**
	 * Close the preferences window and discard all changes
	 */
	@FXML
	public void cancel() {
		preferenceswindow.close();
	}
	
	/**
	 * Close the preferences window and save all changes
	 */
	@FXML
	public void applyChanges() {
		if (potreelocationTextField.getText() != null) {
			preferenceswindow.getPref().changePreference("potreeLocation", potreelocationTextField.getText());
			if (outputfolderTextField.getText() != null) {
				preferenceswindow.getPref().changePreference("outputFolder", outputfolderTextField.getText());
			}
			if (lastoolsbinlocationTextField.getText() != null){
				preferenceswindow.getPref().changePreference("lastoolsBinLocation", lastoolsbinlocationTextField.getText());				
			}
			if (htmlNameTextField.getText() != null) {
				preferenceswindow.getPref().changePreference("defaultHtmlPageName", htmlNameTextField.getText());
			}
			if (browserTextField.getText() != null) {
				preferenceswindow.getPref().changePreference("defaultBrowserPage", browserTextField.getText());
			}
			if (convertingThreadsTextField.getText() != null) {
				preferenceswindow.getPref().changePreference("convertingThreadsAmount", convertingThreadsTextField.getText());
			}
			preferenceswindow.getPref().updatePreferences();
			preferenceswindow.close();
		}else {
			AlertPopup.displayEmptyFieldsError();
		}
	}
	
	public void setPreferenceswindow(PreferencesWindow preferenceswindow) {
		this.preferenceswindow = preferenceswindow;
	}

}
