package Controller;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import Model.AnnotationsHandler;
import Model.CommandRunner;
import Model.MultipleCloudsMerger;
import Model.Observer;
import Model.TemplateHandler;
import View.AlertPopup;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

/**
 * Project Graphical User Interface for Potree Converter
 *
 * @author Van Heirstraeten Arthur
 * Ecole Polytechnique de Bruxelles
 * 
 * Lisa Department
 *
 * Controller for optionbar
 * Divided into 5 section:
 * - Converting buttons
 * - Converting tabs
 * - Template tab
 * - Annotations tab
 * - Multiple pointclouds tab
 * Uses JavaFX
 */
public class OptionBarController extends BaseController {
		
	private Observer observer;
	
	public OptionBarController() {
	}
			
	public void setObserver(Observer observer) {
		this.observer = observer;
	}
	
	public void setHtmlFileName(String file_name) {
		htmlTextField.setText(file_name);
	}
	
	/*
	 * ------------------------- Converting buttons ----------------------------------
	 */
	
	private ArrayList<ArrayList<String>> configurations = new ArrayList<ArrayList<String>>(); //0:conversion number, 1: file name, 2: command
	private Boolean configurations_executed = false;
	private Boolean multithreading_mode = false;
	private List<File> selected_files = new ArrayList<File>();
	private int potree_conversions = 0;
	
	@FXML private TextField fileName;
	@FXML private Button saveConfigurationButton;
	@FXML private Button runButton;
	@FXML private CheckBox multithreadingModeCheckBox;
	@FXML private ListView<Label> queuedFilesListView;
	
	/**
	 * Set the selected files for conversion
	 * @param files
	 */
	public void setSelectedFiles(List<File> files) {
		if (files!=null) {
			selected_files = files;
			if (selected_files.size()==1) {
				fileName.setText(selected_files.get(0).getName());
			}else {
				fileName.setText(selected_files.size() + " files selected");
			}
			saveConfigurationButton.setDisable(false);
		}
	}
	
	/**
	 * Converts optionbar arguments to a command to be executed on terminal
	 * 2 possible choices:
	 * First is when the user uses the multithreading mode
	 * Second is when the user does not use multithreading mode
	 */
	@FXML
	public void runPotreeConverter() {
		observer.notifyUpdateArguments();
		if (AlertPopup.displayConverting()) {
			if (configurations.size()!=0) {
				if (multithreading_mode) {
					
					ExecutorService pool = Executors.newFixedThreadPool(observer.getPreferences().converting_threads_amount);
					
					for(int i = 1; i <= observer.getPreferences().converting_threads_amount; i++){
			            Runnable r = new Runnable(){
			                public void run() {
			                	while (configurations.size()>0) {
			                		ArrayList<String> elem = configurations.remove(0);
			                		
			                		// If next command is a e572las command, execute both conversions on same thread
			                		if (elem.get(0)==null) {
				                		ArrayList<String> elem2 = configurations.remove(0);
										CommandRunner.runConversionCommand(elem.get(2));
										Boolean status = CommandRunner.runCommand(elem2.get(1), elem2.get(2));
										observer.getOptionBarController().setSuccess(Integer.valueOf(elem2.get(0)), status);
									}else {
										Boolean status = CommandRunner.runCommand(elem.get(1), elem.get(2));
										observer.getOptionBarController().setSuccess(Integer.valueOf(elem.get(0)), status);
									}
			                	}
			                }
			            };
			            pool.execute(r);
			        }
					configurations_executed = true;
					potree_conversions = 0;
				}
				else {

				Thread conversion_thread = new Thread(new Runnable() {
					
					@Override
					public void run() {
						for (ArrayList<String> elem:configurations) {
							if (elem.get(0)==null) {
								CommandRunner.runConversionCommand(elem.get(2));
							}else {
								Boolean status = CommandRunner.runCommand(elem.get(1), elem.get(2));
								observer.getOptionBarController().setSuccess(Integer.valueOf(elem.get(0)), status);
							}
						}

						configurations.clear();
						configurations_executed = true;
						potree_conversions = 0;
					}
				});
				conversion_thread.start();
				}
			}
			runButton.setDisable(true);
		
		}
		
	}
	
	/**
	 * Save the actual configuration for running in a batch
	 * Create a command for each file in the selected_files list 
	 */
	@FXML
	public void saveConfiguration() {
		if (configurations_executed) {
			observer.getOptionBarController().clearQueuedFile();
			configurations_executed = false;
		}
		observer.notifyUpdateArguments();
		int elements_count = 0;
		String html_filename = observer.getArguments().getHtmlFileName();
		for (File file : selected_files) {
			
			String new_html_filename = html_filename;
			observer.getArguments().setFileToConvert(file.getAbsolutePath());
			if (!observer.getArguments().getIncremental() && selected_files.size() != 1) {
				new_html_filename += elements_count;
			}
			observer.getOptionBarController().addQueuedFile(file.getName() + " as " + new_html_filename);
			
			if (observer.getArguments().getFileToConvert().replaceAll(".*\\.", "").equals("e57")) {
				configurations.add(new ArrayList<String>(Arrays.asList(null, null, observer.getArguments().getConversionCommand())));
				String new_file = observer.getArguments().getFileToConvert().substring(0, observer.getArguments().getFileToConvert().length()-4) + "_1.las";
				observer.getArguments().setFileToConvert(new_file);					
			}
			configurations.add(new ArrayList<String>(Arrays.asList(Integer.toString(potree_conversions), new_html_filename, observer.getArguments().getCommand(new_html_filename))));
			potree_conversions++;
			System.out.println(configurations.get(configurations.size()-1).get(2));
			elements_count ++;
		}
		runButton.setDisable(false);
	}
	
	/**
	 * Delete selected configuration
	 */
	@FXML
	public void deleteConfiguration() {
		int index = queuedFilesListView.getSelectionModel().getSelectedIndex();
		if (index != -1 && !queuedFilesListView.getItems().get(index).getText().contains("Removed")) {
			String name = queuedFilesListView.getItems().get(index).getText() + " - Removed";
			queuedFilesListView.getItems().set(index, new Label(name));
			queuedFilesListView.getItems().get(index).setTextFill(Color.RED);
			int i = 0;
			while (configurations.get(i).get(0) == null || Integer.valueOf(configurations.get(i).get(0)) != index) {
				i++;
			}
			configurations.remove(i);
			if (i > 0 && configurations.get(i-1).get(0) == null) {
				configurations.remove(i-1);
			}
			if (configurations.size() == 0) {
				runButton.setDisable(true);
			}
		}
	}
	
	/**
	 * Clear All configurations
	 */
	@FXML
	public void clearAllConfigurations() {
		Boolean confirmation = AlertPopup.displayClearAll();
		if (confirmation) {
			configurations.clear();
			queuedFilesListView.getItems().clear();
		}
		runButton.setDisable(true);
	}
	
	/**
	 * Toggle the multithreading mode
	 */
	@FXML
	public void toggleMultithreadingMode() {
		if(multithreading_mode) {
			multithreading_mode = false;
		}else {
			Boolean status = AlertPopup.displayMultithreadingMode();
			multithreading_mode = status;
			multithreadingModeCheckBox.setSelected(status);
		}
	}	
	
	/*
	 * --------------------------- Converting tabs -----------------------------------
	 */
	
	@FXML private CheckBox htmlCheckBox;
	@FXML private CheckBox spacingCheckBox;
	@FXML private CheckBox diagonalFractionCheckBox;
	@FXML private CheckBox levelsCheckBox;
	@FXML private CheckBox inputFormatCheckBox;
	@FXML private CheckBox colorRangeCheckBox;
	@FXML private CheckBox intensityRangeCheckBox;
	@FXML private CheckBox outputFormatCheckBox;
	@FXML private CheckBox outputAttributesCheckBox;
	@FXML private CheckBox outputAttributesRGBCheckBox;
	@FXML private CheckBox outputAttributesINTENSITYCheckBox;
	@FXML private CheckBox outputAttributesCLASSIFICATIONCheckBox;
	@FXML private CheckBox scaleCheckBox;
	@FXML private CheckBox aabbCheckBox;
	@FXML private CheckBox incrementalCheckBox;
	@FXML private CheckBox overwriteCheckBox;
	@FXML private CheckBox sourceListingCheckBox;	
	@FXML private CheckBox projectionCheckBox;
	@FXML private CheckBox listOfFilesCheckBox;
	@FXML private CheckBox titleCheckBox;
	@FXML private CheckBox descriptionCheckBox;
	@FXML private CheckBox edlEnabledCheckBox;
	@FXML private CheckBox showSkyboxCheckBox;
	@FXML private CheckBox materialCheckBox;
	@FXML private CheckBox materialRGBCheckBox;
	@FXML private CheckBox materialELEVATIONCheckBox;
	@FXML private CheckBox materialINTENSITYCheckBox;
	@FXML private CheckBox materialINTENSITY_GRADIENTCheckBox;
	@FXML private CheckBox materialRETURN_NUMBERCheckBox;
	@FXML private CheckBox materialSOURCECheckBox;
	@FXML private CheckBox materialLEVEL_OF_DETAILCheckBox;	
	@FXML private CheckBox storeSizeCheckBox;
	@FXML private CheckBox flushLimitCheckBox;
	
	@FXML private TextField htmlTextField;
	@FXML private TextField spacingTextField;
	@FXML private TextField diagonalFractionTextField;
	@FXML private TextField levelsTextField;
	@FXML private TextField inputFormatTextField;
	@FXML private TextField colorRangeTextField;
	@FXML private TextField intensityRangeTextField;
	@FXML private TextField outputFormatTextField;
	@FXML private TextField scaleTextField;
	@FXML private TextField aabbMinTextField;
	@FXML private TextField aabbMaxTextField;
	@FXML private TextField projectionTextField;
	@FXML private TextField listOfFilesTextField;
	@FXML private TextField titleTextField;
	@FXML private TextField descriptionTextField;
	@FXML private TextField storeSizeTextField;
	@FXML private TextField flushLimitTextField;
	@FXML private TextField outputFolderTextField;
	
	/**
	 * Initiliaze the output field to the default value specified 
	 * in the preferences
	 */
	public void initializeFields() {
		outputFolderTextField.setText(observer.getPreferences().output_folder);
	}
	
	/**
	 * Toggle a dialog for the user to select the output folder 
	 * for the current conversion
	 */
	@FXML public void browseOutputFolder() {
		DirectoryChooser directory_chooser = new DirectoryChooser();
		if (outputFolderTextField.getText() != null && new File(outputFolderTextField.getText()).isDirectory()) {
			directory_chooser.setInitialDirectory(new File(outputFolderTextField.getText()));
		}else {
			directory_chooser.setInitialDirectory(new File(System.getProperty("user.home")));
		}
		directory_chooser.setTitle("Select default output folder");
		File dir = directory_chooser.showDialog(stage);
		if (dir!=null) {
			outputFolderTextField.setText(dir.getAbsolutePath());
		}
	}
	
	/**
	 * All functions below update arguments selection and enable/disable TextFields
	 */
	
	@FXML public void updateHtmlSelection() {
		htmlTextField.setDisable(!htmlCheckBox.isSelected());
		observer.getArguments().setOutputAsHtml(htmlCheckBox.isSelected());
	}	
	
	@FXML public void updateSpacingSelection() {
		spacingTextField.setDisable(!spacingCheckBox.isSelected());
		observer.getArguments().setSpacing(spacingCheckBox.isSelected());
	}
	
	@FXML public void updateDiagonalFractionSelection() {
		diagonalFractionTextField.setDisable(!diagonalFractionCheckBox.isSelected());
		observer.getArguments().setDiagonalFraction(diagonalFractionCheckBox.isSelected());
	}
	
	@FXML public void updateLevelsSelection() {
		levelsTextField.setDisable(!levelsCheckBox.isSelected());
		observer.getArguments().setLevels(levelsCheckBox.isSelected());
	}
	
	@FXML public void updateInputFormatSelection() {
		inputFormatTextField.setDisable(!inputFormatCheckBox.isSelected());
		observer.getArguments().setInputFormat(inputFormatCheckBox.isSelected());
	}
	
	@FXML public void updateColorRangeSelection() {
		colorRangeTextField.setDisable(!colorRangeCheckBox.isSelected());
		observer.getArguments().setColorRange(colorRangeCheckBox.isSelected());
	}
	
	@FXML public void updateIntensityRangeSelection() {
		intensityRangeTextField.setDisable(!intensityRangeCheckBox.isSelected());
		observer.getArguments().setIntensityRange(intensityRangeCheckBox.isSelected());
	}
	
	@FXML public void updateOutputFormatSelection() {
		outputFormatTextField.setDisable(!outputFormatCheckBox.isSelected());
		observer.getArguments().setOutputFormat(outputFormatCheckBox.isSelected());
	}
	
	@FXML public void updateOutputAttributesSelection() {
		outputAttributesRGBCheckBox.setDisable(!outputAttributesCheckBox.isSelected());
		outputAttributesINTENSITYCheckBox.setDisable(!outputAttributesCheckBox.isSelected());
		outputAttributesCLASSIFICATIONCheckBox.setDisable(!outputAttributesCheckBox.isSelected());
		observer.getArguments().setOutputAttributes(outputAttributesCheckBox.isSelected());
	}
	
	@FXML public void updateOutputAttributesRGBSelection() {
		observer.getArguments().setOutputAttributesRGB(outputAttributesRGBCheckBox.isSelected());
	}

	@FXML public void updateOutputAttributesINTENSITYSelection() {
		observer.getArguments().setOutputAttributesINTENSITY(outputAttributesINTENSITYCheckBox.isSelected());
	}

	@FXML public void updateOutputAttributesCLASSIFICATIONSelection() {
		observer.getArguments().setOutputAttributesCLASSIFICATION(outputAttributesCLASSIFICATIONCheckBox.isSelected());
	}
	
	
	@FXML public void updateScaleSelection() {
		scaleTextField.setDisable(!scaleCheckBox.isSelected());
		observer.getArguments().setScale(scaleCheckBox.isSelected());
	}
	
	@FXML public void updateAabbSelection() {
		aabbMinTextField.setDisable(!aabbCheckBox.isSelected());
		aabbMaxTextField.setDisable(!aabbCheckBox.isSelected());
		observer.getArguments().setAabb(aabbCheckBox.isSelected());
	}
	
	@FXML public void updateIncrementalSelection() {
		observer.getArguments().setIncremental(incrementalCheckBox.isSelected());
	}
	
	@FXML public void updateOverwriteSelection() {
		observer.getArguments().setOverwrite(overwriteCheckBox.isSelected());
	}
	
	@FXML public void updateSourceListingSelection() {
		observer.getArguments().setSourceListingOnly(sourceListingCheckBox.isSelected());
	}
	
	@FXML public void updateProjectionSelection() {
		projectionTextField.setDisable(!projectionCheckBox.isSelected());
		observer.getArguments().setProjection(projectionCheckBox.isSelected());
	}
	
	@FXML public void updateListOfFilesSelection() {
		listOfFilesTextField.setDisable(!listOfFilesCheckBox.isSelected());
		observer.getArguments().setListOfFiles(listOfFilesCheckBox.isSelected());
	}
	
	@FXML public void updateTitleSelection() {
		titleTextField.setDisable(!titleCheckBox.isSelected());
		observer.getArguments().setTitle(titleCheckBox.isSelected());
	}
	
	@FXML public void updateDescriptionSelection() {
		descriptionTextField.setDisable(!descriptionCheckBox.isSelected());
		observer.getArguments().setDescription(descriptionCheckBox.isSelected());
	}
	
	@FXML public void updateEdlEnabledSelection() {
		observer.getArguments().setEdlEnabled(edlEnabledCheckBox.isSelected());
	}
	
	@FXML public void updateShowSkyboxSelection() {
		observer.getArguments().setShowSkybox(showSkyboxCheckBox.isSelected());
	}
	
	@FXML public void updateMaterialSelection() {
		ArrayList<CheckBox> material_types = new ArrayList<CheckBox>();
		material_types.addAll(Arrays.asList(materialRGBCheckBox,materialELEVATIONCheckBox, materialINTENSITYCheckBox, 
				materialINTENSITY_GRADIENTCheckBox, materialRETURN_NUMBERCheckBox, materialSOURCECheckBox, materialLEVEL_OF_DETAILCheckBox));
		for (CheckBox type:material_types){
			type.setDisable(!materialCheckBox.isSelected());
		}
		observer.getArguments().setMaterial(materialCheckBox.isSelected());
	}
	
	@FXML public void updateMaterialRGBSelection() {
		if(materialRGBCheckBox.isSelected()) {
			observer.getArguments().addMaterialArg("RGB");
		}else {
			observer.getArguments().removeMaterialArg("RGB");
		}
	}
	
	@FXML public void updateMaterialELEVATIONSelection() {
		if(materialELEVATIONCheckBox.isSelected()) {
			observer.getArguments().addMaterialArg("ELEVATION");
		}else {
			observer.getArguments().removeMaterialArg("ELEVATION");
		}
	}
	
	@FXML public void updateMaterialINTENSITYSelection() {
		if(materialINTENSITYCheckBox.isSelected()) {
			observer.getArguments().addMaterialArg("INTENSITY");
		}else {
			observer.getArguments().removeMaterialArg("INTENSITY");
		}
	}
	
	@FXML public void updateMaterialINTENSITY_GRADIENTSelection() {
		if(materialINTENSITY_GRADIENTCheckBox.isSelected()) {
			observer.getArguments().addMaterialArg("INTENSITY_GRADIENT");
		}else {
			observer.getArguments().removeMaterialArg("INTENSITY_GRADIENT");
		}
	}
	
	@FXML public void updateMaterialRETURN_NUMBERSelection() {
		if(materialRETURN_NUMBERCheckBox.isSelected()) {
			observer.getArguments().addMaterialArg("RETURN_NUMBER");
		}else {
			observer.getArguments().removeMaterialArg("RETURN_NUMBER");
		}
	}
	
	@FXML public void updateMaterialSOURCESelection() {
		if(materialSOURCECheckBox.isSelected()) {
			observer.getArguments().addMaterialArg("SOURCE");
		}else {
			observer.getArguments().removeMaterialArg("SOURCE");
		}
	}
	
	@FXML public void updateMaterialLEVEL_OF_DETAILSelection() {
		if(materialLEVEL_OF_DETAILCheckBox.isSelected()) {
			observer.getArguments().addMaterialArg("LEVEL_OF_DETAIL");
		}else {
			observer.getArguments().removeMaterialArg("LEVEL_OF_DETAIL");
		}
	}
	
	@FXML public void updateStoreSizeSelection() {
		storeSizeTextField.setDisable(!storeSizeCheckBox.isSelected());
		observer.getArguments().setStoreSize(storeSizeCheckBox.isSelected());
	}
	
	@FXML public void updateFlushLimitSelection() {
		flushLimitTextField.setDisable(!flushLimitCheckBox.isSelected());
		observer.getArguments().setFlushLimit(flushLimitCheckBox.isSelected());
	}
	
	
	/**
	 * Put optionbar parameters in the Arguments object
	 */
	public void saveArguments() {
		observer.getArguments().setOutputFolder(outputFolderTextField.getText());
		observer.getArguments().setHtmlFileName(htmlTextField.getText());
		observer.getArguments().setSpacingArgs(spacingTextField.getText());
		observer.getArguments().setDiagonalFractionArgs(diagonalFractionTextField.getText());
		observer.getArguments().setLevelsArgs(levelsTextField.getText());
		observer.getArguments().setInputFormatArgs(inputFormatTextField.getText());
		observer.getArguments().setColorRangeArgs(colorRangeTextField.getText());
		observer.getArguments().setIntensityRangeArgs(intensityRangeTextField.getText());
		observer.getArguments().setOutputFormatArgs(outputFormatTextField.getText());
		observer.getArguments().setScaleArgs(scaleTextField.getText());
		observer.getArguments().setAabbMinArgs(aabbMinTextField.getText());
		observer.getArguments().setAabbMaxArgs(aabbMaxTextField.getText());
		observer.getArguments().setProjectionArgs(projectionTextField.getText());
		observer.getArguments().setListOfFilesName(listOfFilesTextField.getText());
		observer.getArguments().setTitleArgs(titleTextField.getText());
		observer.getArguments().setDescriptionArgs(descriptionTextField.getText());
		observer.getArguments().setStoreSizeArgs(storeSizeTextField.getText());
		observer.getArguments().setFlushLimitArgs(flushLimitTextField.getText());
	}
	
	/**
	 * Add a file to the queue ListView
	 * @param file_name
	 */
	public void addQueuedFile(String file_name) {
		queuedFilesListView.getItems().add(new Label(file_name));
		queuedFilesListView.refresh();
	}
	
	/**
	 * Change the color of an element in the queue ListView 
	 * corresponding to succes or fail of conversion
	 * @param item_number
	 * @param status
	 */
	public void setSuccess(int item_number, Boolean status) {
		if (status) {
			queuedFilesListView.getItems().get(item_number).setTextFill(Color.GREEN);
		}else {
			queuedFilesListView.getItems().get(item_number).setTextFill(Color.RED);
		}
		queuedFilesListView.refresh();
	}
	
	/**
	 * Clear the queue ListView
	 */
	public void clearQueuedFile() {
		queuedFilesListView.getItems().clear();
		queuedFilesListView.refresh();
	}
	

	/*
	 * ----------------------------- Template tab ------------------------------------
	 */

	private TemplateHandler template_handler;
	@FXML private Button applyTemplateButton;
	@FXML private Label templateFileNameLabel;
	@FXML private TextField FOVTextField;
	@FXML private TextField pointBudgetTextField;
	@FXML private TextField languageTextField;
	@FXML private TextField backgroundTextField;
	@FXML private TextField pointSizeTextField;
	
	@FXML private CheckBox hideAppearanceMenu;
	@FXML private CheckBox hideToolsMenu;
	@FXML private CheckBox hideSceneMenu;
	@FXML private CheckBox hideClassificationMenu;
	@FXML private CheckBox hideAboutMenu;
	@FXML private CheckBox closeAppearanceMenu;
	@FXML private CheckBox closeToolsMenu;
	@FXML private CheckBox closeSceneMenu;
	@FXML private CheckBox closeClassificationMenu;
	@FXML private CheckBox closeAboutMenu;
	@FXML private CheckBox toggleSidebarCheckBox;
	
	/**
	 * Apply template to the file
	 */
	@FXML
	public void applyTemplate() {
		if (template_handler != null) {
			template_handler.applyChanges(FOVTextField.getText(), pointBudgetTextField.getText(), languageTextField.getText(),
					backgroundTextField.getText(), pointSizeTextField.getText());
			template_handler.toggleMenus(hideAppearanceMenu.isSelected(), hideToolsMenu.isSelected(), hideSceneMenu.isSelected(), 
					hideClassificationMenu.isSelected(), hideAboutMenu.isSelected(), closeAppearanceMenu.isSelected(), 
					closeToolsMenu.isSelected(), closeSceneMenu.isSelected(), closeClassificationMenu.isSelected(), 
					closeAboutMenu.isSelected(), toggleSidebarCheckBox.isSelected());
			template_handler.writeToFile();
			AlertPopup.displayInformationMessage("Template applied successfully");
		}
	}
	
	/**
	 * Toggle a dialog for the user to choose the pointcloud on
	 * which he wants to apply the template
	 */
	@FXML
	public void openCloudForTemplate() {
		FileChooser filechooser = new FileChooser();
		filechooser.setInitialDirectory(new File(System.getProperty("user.home")));
		filechooser.setTitle("Select a file");
		File file = filechooser.showOpenDialog(stage);
		if (file!=null) {
			template_handler = new TemplateHandler(file.getAbsolutePath());
			templateFileNameLabel.setText(file.getName());
			applyTemplateButton.setDisable(false);
		}
	}
	
	/*
	 * --------------------------- Annotations tab -----------------------------------
	 */
	
	private AnnotationsHandler annotations_handler;
	@FXML private Label annotationsFileNameLabel;
	@FXML private TextField annotationsNameTextField = new TextField("");
	@FXML private TextField annotationsTitleTextField = new TextField("");
	@FXML private TextField annotationsCoordinatesTextField = new TextField("");
	@FXML private TextArea annotationsDescriptionTextArea = new TextArea("");
	@FXML private Button applyAnnotationsButton;
	@FXML private Button addAnnotationButton;
	@FXML private ListView<String> annotationsList;
	private Boolean annotations_added = false;
	
	/**
	 * Add annotations to the file
	 */
	@FXML
	public void applyAnnotations() {
		if (annotations_handler != null) {
			annotations_handler.writeToFile();
			AlertPopup.displayInformationMessage("Annotations added successfully");
			annotations_added = true;
			applyAnnotationsButton.setDisable(true);
		}
	}
	
	/**
	 * Toggle a dialog for the user to choose the pointcloud on
	 * which he wants to apply annotations
	 */
	@FXML
	public void openCloudForAnnotations() {
		FileChooser filechooser = new FileChooser();
		filechooser.setInitialDirectory(new File(System.getProperty("user.home")));
		filechooser.setTitle("Select a file");
		File file = filechooser.showOpenDialog(stage);
		if (file!=null) {
			annotations_handler = new AnnotationsHandler(file.getAbsolutePath());
			annotationsFileNameLabel.setText(file.getName());
			addAnnotationButton.setDisable(false);
			applyAnnotationsButton.setDisable(false);
			annotationsList.getItems().clear();
			annotations_added = false;
		}
	}
	
	/**
	 * Add the current annotation to the queue of annotations
	 */
	@FXML
	public void addAnnotation() {
		if (annotations_added) {
			annotationsList.getItems().clear();
			annotations_handler.reset();
			annotations_added = false;
			applyAnnotationsButton.setDisable(false);
		}
		if (annotationsNameTextField.getText()!= "" && annotationsTitleTextField.getText()!= "" 
				&& annotationsCoordinatesTextField.getText()!= "") {
			annotations_handler.addAnnotation(annotationsNameTextField.getText(), annotationsTitleTextField.getText(),
					annotationsCoordinatesTextField.getText(), annotationsDescriptionTextArea.getText());
			annotationsList.getItems().add(annotationsTitleTextField.getText() + 
					" at " + annotationsCoordinatesTextField.getText());	
			}
	}
	

	/*
	 * ------------------------ Multiple pointclouds tab -------------------------------
	 */
	
	@FXML private ListView<String> multiCloudsListView;
	private ArrayList<String> paths_for_merging = new ArrayList<String>();
	

	/**
	 * Toggle a dialog for the user to choose a pointcloud he wants to merge
	 * File is then added to queue for merging
	 */
	@FXML
	public void multiCloudsAddFile() {
		FileChooser filechooser = new FileChooser();
		filechooser.setInitialDirectory(new File(System.getProperty("user.home")));
		filechooser.setTitle("Select file for merging");
		List<File> files = filechooser.showOpenMultipleDialog(stage);
		if (files!=null) {
			for (File file:files) {
				paths_for_merging.add(file.getAbsolutePath());
				multiCloudsListView.getItems().add(file.getName());
			}
		}
	}
	
	/**
	 * Merge all files in the queue and output them under 
	 * the name requested by the user
	 */
	@FXML
	public void multiCloudsMerge() {
		if (paths_for_merging.size()>1) {

			FileChooser filechooser = new FileChooser();
			filechooser.setInitialDirectory(new File(observer.getPreferences().output_folder));
			//Set extension filter for html files
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("HTML files (*.html)", "*.html");
            filechooser.getExtensionFilters().add(extFilter);
			filechooser.setTitle("Save as");
			File file = filechooser.showSaveDialog(stage);
			if (file!=null) {
				String main_file_name = paths_for_merging.remove(0);
				MultipleCloudsMerger merger = new MultipleCloudsMerger (observer.getPreferences(), main_file_name);
				for (String elem:paths_for_merging) {
					merger.addCloud(elem);
				}
				merger.merge(file.getAbsolutePath());
				paths_for_merging.add(0, main_file_name);
			}
			
		}else {
			AlertPopup.displayNumberOfFilesForMergingError();
		}
	}
	
	/**
	 * Remove one element from the queue
	 */
	@FXML
	public void multiCloudsRemove() {
		int index = multiCloudsListView.getSelectionModel().getSelectedIndex();
		if (index != -1) {
			paths_for_merging.remove(index);
			multiCloudsListView.getItems().remove(index);
		}
	}
	

	
	/**
	 * Clear the current queue
	 */
	@FXML
	public void multiCloudsClearAll() {
		Boolean confirmation = AlertPopup.displayClearAll();
		if (confirmation) {
			paths_for_merging.clear();
			multiCloudsListView.getItems().clear();
		}
	}

}
