package View;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;

/**
 * Project Graphical User Interface for Potree Converter
 *
 * @author Van Heirstraeten Arthur
 * Ecole Polytechnique de Bruxelles
 * 
 * Lisa Department
 *
 * All popup windows/dialog
 */
public class AlertPopup{
		
	/**
	 * Display an IOException in a dialog
	 * @param e
	 */
	public static void displayRuntimeError(IOException e) {
	Alert alert = new Alert(AlertType.ERROR);
	alert.setTitle("Error");
	alert.setHeaderText("There was an error at runtime");
	StringWriter sw = new StringWriter();
	e.printStackTrace(new PrintWriter(sw));
	alert.setContentText(sw.toString());
	alert.showAndWait();
	}
	
	/**
	 * Display a warning for empty fields
	 */
	public static void displayEmptyFieldsError() {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Warning");
		alert.setHeaderText("Please at least select PotreeConverter.exe");
		alert.showAndWait();
	}
	
	/**
	 * Display a warning for the minimum amount of required files not reached
	 */
	public static void displayNumberOfFilesForMergingError() {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Warning");
		alert.setHeaderText("You need at least two files for merging !");
		alert.showAndWait();
	}
	
	/**
	 * Display an error if JxBrowser license is not valid
	 */
	public static void displayJxBrowserLicenseError() {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Warning");
		alert.setHeaderText("This program uses the JxBrowser library from TeamDev but no valid license was found !\n"
				+ "You can proceed to launch but no preview will be available.");
		alert.showAndWait();
	}
	
	/**
	 * Display a confirmation dialog box before launching the conversion
	 * @return confirmation
	 */
	public static Boolean displayConverting() {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Conversion");
		alert.setHeaderText("Do you want to start conversion ?");
		alert.setContentText("Please wait all conversions are done before adding new ones");
		alert.getButtonTypes().setAll(ButtonType.OK, ButtonType.CANCEL);
		Optional<ButtonType> result = alert.showAndWait();
		return result.get() == ButtonType.OK;
	}
	
	/**
	 * Display a confirmation dialog box before switching to multithreading mode
	 * @return confirmation
	 */
	public static Boolean displayMultithreadingMode() {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Multithreading mode");
		alert.setHeaderText("Do you really want to switch to multithreading mode?");
		alert.setContentText("Using this mode, incremental conversions may not work!");
		alert.getButtonTypes().setAll(ButtonType.OK, ButtonType.CANCEL);
		Optional<ButtonType> result = alert.showAndWait();
		return result.get() == ButtonType.OK;

	}
	
	/**
	 * Display a confirmation dialog box before clearing all the list
	 * @return confirmation
	 */
	public static Boolean displayClearAll() {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Clear All");
		alert.setHeaderText("Are you sure you want to clear the whole list?");
		alert.getButtonTypes().setAll(ButtonType.OK, ButtonType.CANCEL);
		Optional<ButtonType> result = alert.showAndWait();
		return result.get() == ButtonType.OK;

	}
	
	/**
	 * Display a dialog specifying the status of the conversion
	 * @param status
	 */
	public static void displayConversionFinished(Boolean status) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Conversion");
		if (status) {
			alert.setHeaderText("Conversion successfull");
		}else {
			alert.setHeaderText("Conversion failed");
		}
		alert.showAndWait();
	}
	
	/**
	 * Display a dialog specifying the amount of successful conversions
	 * @param success_elements
	 * @param number_of_elements
	 */
	public static void displayBatchConversionFinished(int success_elements, int number_of_elements) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Conversion");
		alert.setHeaderText(success_elements + " out of " + number_of_elements + " were successfully converted");
		alert.showAndWait();
	}
	
	/**
	 * Open a dialog for user input of url address
	 * @return url
	 */
	public static String requestURL() {
		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle("Open URL");
		dialog.setHeaderText("Please enter the URL you want to open");
		dialog.setContentText("URL:");
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {
			return result.get();
		}else {
			return null;
		}		
	}
	
	/**
	 * Open a dialog for output file name input
	 * @return output file name
	 */
	public static String requestOutputName() {
		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle("Merge multiple pointclouds");
		dialog.setHeaderText("Please enter the final name of your html page");
		dialog.setContentText("filename:");
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {
			return result.get();
		}else {
			return null;
		}		
	}
	
	/**
	 * Display a dialog with a specified message
	 * @param message
	 */
	public static void displayInformationMessage(String message) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Information");
		alert.setHeaderText(message);
		alert.showAndWait();
	}

}
