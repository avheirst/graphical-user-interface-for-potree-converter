package View;

import java.io.IOException;

import Controller.BaseController;
import Model.Constants;
import Model.Observer;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

/**
 * Project Graphical User Interface for Potree Converter
 *
 * @author Van Heirstraeten Arthur
 * Ecole Polytechnique de Bruxelles
 * 
 * Lisa Department
 *
 * Abstract class for main window elements
 */
public abstract class BaseWindow extends AnchorPane {
	protected BaseController controller;
	protected Observer observer;
	
	/**
	 * link controller and load corresponding xml file
	 * @param file_name
	 * @param controller
	 */
	public BaseWindow(String file_name, BaseController controller, Observer observer) {
		this.controller = controller;
		this.observer = observer;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(Constants.relPathToResources + file_name));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(controller);

		try {
			fxmlLoader.load();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}
	
	public void setObserver(Observer observer) {
		controller.setObserver(observer);
	}
	
	public BaseController getController() {
		return controller;
	}
}
