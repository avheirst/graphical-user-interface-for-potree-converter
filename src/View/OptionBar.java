package View;

import Controller.OptionBarController;
import Model.Observer;
import javafx.stage.Stage;

/**
 * Project Graphical User Interface for Potree Converter
 *
 * @author Van Heirstraeten Arthur
 * Ecole Polytechnique de Bruxelles
 * 
 * Lisa Department
 *
 * Option bar window
 */
public class OptionBar extends BaseWindow {
	
	private static OptionBarController controller = new OptionBarController();

	public OptionBar(Observer observer) {
		super("optionbar.fxml", controller, observer);
		controller.setObserver(observer);
		controller.initializeFields();
		if (observer.getPreferences().default_html_name!=null) {
			controller.setHtmlFileName(observer.getPreferences().default_html_name);
		}
	}
	
	/**
	 * Intialize the object
	 * @param stage
	 */
	public void initialize(Stage stage) {
		controller.bindHeight(stage);
	}
	
}
