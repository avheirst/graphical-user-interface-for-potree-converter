package View;

import java.io.IOException;
import Controller.PreferencesWindowController;
import Model.Constants;
import Model.Preferences;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Project Graphical User Interface for Potree Converter
 *
 * @author Van Heirstraeten Arthur
 * Ecole Polytechnique de Bruxelles
 * 
 * Lisa Department
 *
 * Preferences window
 * Opens in a new window
 */
public class PreferencesWindow {
	
	private Preferences pref;
	private Stage stage = new Stage();
	
	private static PreferencesWindowController controller = new PreferencesWindowController();

	/**
	 * Load fxml file and initialize controller
	 * @param pref
	 */
	public PreferencesWindow(Preferences pref) {
		this.pref = pref;
		Parent root;
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource(Constants.relPathToResources + "preferenceswindow.fxml"));
			loader.setController(controller);
			root = loader.load();
			stage.getIcons().add(new Image(Constants.iconFile));
			stage.setTitle("Preferences");
			stage.setScene(new Scene(root,500,500));
			stage.show();
		} catch (IOException e) {
			e.printStackTrace();
		};
		
		controller.setPreferenceswindow(this);
		controller.initializeFields();
	}
	
	/**
	 * close the window
	 */
	public void close() {
		stage.close();
	}
	
	public Preferences getPref() {
		return pref;
	}
	
	
	
}
