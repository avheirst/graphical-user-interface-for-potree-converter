package View;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.javafx.BrowserView;

/**
 * Project Graphical User Interface for Potree Converter
 *
 * @author Van Heirstraeten Arthur
 * Ecole Polytechnique de Bruxelles
 * 
 * Lisa Department
 * 
 * Browser class
 * This version implements JxBrowser 6.x
 */
public class PreviewBrowser {

	private Browser browser;
    private BrowserView browser_view;
    private Boolean running;

	public PreviewBrowser() {
		try {
			browser = new Browser();
			browser_view = new BrowserView(browser);
			running = true;
		}catch (Error e) {
			System.out.println("License not valid");
			AlertPopup.displayJxBrowserLicenseError();
			running = false;
		}
	}
	
	/**
	 * Load a page URL
	 * @param url
	 */
	public void loadURL(String url) {
		if (running) {
			browser.loadURL(url);
        }
	}
	
	/**
	 * Toggle a dialog for a new URL to open
	 */
	public void openNewURL() {
		if (running) {
			String url = AlertPopup.requestURL();
			if (url !=null) {
				browser.loadURL(url);
			}
		}
	}
	
	/**
	 * reload the browser
	 */
	public void refresh() {
		if (running){
			browser.reload();
		}
	}
		
	/**
	 * Stop the browser
	 */
	public void stop() {
		if (running) {
			browser.dispose();
		}
	}
	
	public BrowserView getBrowserView() {
		return browser_view;
	}
	
	public Boolean getRunning() {
		return running;
	}
	
}
