package View;

import Controller.MenuBarController;
import Model.Observer;
import javafx.stage.Stage;

/**
 * Project Graphical User Interface for Potree Converter
 *
 * @author Van Heirstraeten Arthur
 * Ecole Polytechnique de Bruxelles
 * 
 * Lisa Department
 *
 * Menu bar window
 */
public class MenuBar extends BaseWindow {
		
	private static MenuBarController controller = new MenuBarController();
	
	public MenuBar(Observer observer) {
		super("menubar.fxml", controller, observer);
	}
	
	/**
	 * Initialize the object
	 * @param stage
	 */
	public void initialize(Stage stage) {
		controller.bindWidth(stage);
	}
	
	
}
