package View;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import Model.Arguments;
import Model.Constants;
import Model.Observer;

/**
 * Project Graphical User Interface for Potree Converter
 *
 * @author Van Heirstraeten Arthur
 * Ecole Polytechnique de Bruxelles
 * 
 * Lisa Department
 *
 * Main window of the application
 */
public class MainWindow{
	private Scene scene;
	private BorderPane root;
	private Observer observer;
	private MenuBar menubar;
	private OptionBar optionbar;
	private PreviewBrowser browser;
	
	public MainWindow(Stage stage, Observer observer) {
		this.observer = observer;
		browser = new PreviewBrowser();
		optionbar = new OptionBar(observer);
		menubar = new MenuBar(observer);
		paneSetup();
		stageSetup(stage);
		observer.setArguments(new Arguments(observer));
		observer.setBrowser(browser);
		menubar.initialize(stage);
		optionbar.initialize(stage);
		linkToObserver();
		if(observer.getPreferences().default_browser_page!=null) {
			browser.loadURL(observer.getPreferences().default_browser_page);
		}else {
			browser.loadURL("http://panorama.ulb.ac.be/");
		}
	}	
	
	/**
	 * Dispose all elements on the window
	 */
	private void paneSetup() {
		VBox vbox = new VBox();
		HBox hbox = new HBox();
		if (browser.getRunning()) {
			hbox.getChildren().addAll(optionbar, browser.getBrowserView());
			HBox.setHgrow(browser.getBrowserView(), Priority.ALWAYS);
		}else {
			hbox.getChildren().addAll(optionbar);
		}
		vbox.getChildren().addAll(menubar, hbox);
		root = new BorderPane(vbox);
	}

	/**
	 * Setup the stage of the application
	 * @param stage
	 */
	private void stageSetup(Stage stage) {
		scene = new Scene(root, Constants.WINDOW_WIDTH, Constants.WINDOW_HEIGHT);
		stage.getIcons().add(new Image(Constants.iconFile));
		stage.setTitle(Constants.APP_DISPLAY_NAME);
		stage.setScene(scene);
		stage.show();
	}
	
	/**
	 * Link the observer to all sub windows
	 */
	private void linkToObserver() {
		optionbar.setObserver(observer);
		menubar.setObserver(observer);
		observer.setOptionbarcontroller(optionbar.getController());
		observer.setMenubarcontroller(menubar.getController());
		
	}
	
	/**
	 * toggle interruption of the browser
	 */
	public void stopBrowser() {
		browser.stop();
	}

}
