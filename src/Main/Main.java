package Main;

import javafx.application.Application;
import javafx.stage.Stage;
import View.MainWindow;

import java.io.File;

import Model.Constants;
import Model.Observer;
import Model.Preferences;

/**
 * Project Graphical User Interface for Potree Converter
 *
 * @author Van Heirstraeten Arthur
 * Ecole Polytechnique de Bruxelles
 * 
 * Lisa Department
 *
 * Main class
 */
public class Main extends Application {

	private MainWindow mainwindow;
	private Observer observer = new Observer();

	public static void main(String[] args) {
		File directory = new File(Constants.logPath);
		if (!directory.exists()) {
			directory.mkdir();
		}
		launch(args);
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		observer.setPreferences(new Preferences());
		mainwindow = new MainWindow(stage, observer);
	}
	
	@Override
	public void stop() {
		mainwindow.stopBrowser();
	}

}
