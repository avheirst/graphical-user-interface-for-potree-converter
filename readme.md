# PROJ-H-402 Computing Project - Panotree

This project has been created under the autority of ULB (Université Libre de Bruxelles) for the Master 1 project in Computer Science Engineering.

The project has been developped only for use in the Panorama department of ULB.

Project is working using MIT open source code for Potree Converter (http://www.potree.org/)

Project is working using a licensed library JxBrowser (https://www.teamdev.com/jxbrowser)

Developped using Java 8 and JavaFX 8

---

The developper is Van Heirstraeten Arthur <avheirst@ulb.ac.be>

Supervisors are Ercek Rudy <rercek@ulb.ac.be>, Guillaume Henry-Louis <henguill@ulb.ac.be> and Schenkel Arnaud <arnaud.schenkel@ulb.ac.be>

Academic promoter is Debeir Olivier <odebeir@ulb.ac.be>
